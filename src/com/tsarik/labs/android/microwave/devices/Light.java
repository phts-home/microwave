package com.tsarik.labs.android.microwave.devices;


/**
 * Interface for the Light.
 * 
 * @author Phil Tsarik
 *
 */
public interface Light {
	
	/**
	 * Returns <code>true</code> if the light is switched on.
	 * 
	 * @return <code>true</code> if the light is switched on or <code>false</code> otherwise
	 */
	public boolean isSwitchedOn();
	
	/**
	 * Switches on the light.
	 */
	public void switchOn();
	
	/**
	 * Switches off the light.
	 */
	public void switchOff();
	
}
