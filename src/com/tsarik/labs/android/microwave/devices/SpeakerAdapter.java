package com.tsarik.labs.android.microwave.devices;


/**
 * Interface for the Speaker adapter.
 * 
 * @author Phil Tsarik
 *
 */
public interface SpeakerAdapter {
	
	/**
	 * Plays sound for button pressing.
	 */
	public void playButtonSound();
	
	/**
	 * Plays sound for finishing processing
	 */
	public void playFinishProcessingSound();
	
}
