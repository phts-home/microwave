package com.tsarik.labs.android.microwave.devices;


/**
 * Simple implementation for the <code>IndicatorAdapter</code> interface.
 * 
 * @author Phil Tsarik
 *
 */
class IndicatorAdapterImpl implements IndicatorAdapter {
	
	
	@Override
	public void clear() {
		indicateTime(0);
	}
	
	@Override
	public void indicateTime(int seconds) {
		int minutes = 0;
		while (seconds > 59) {
			minutes++;
			seconds -= 60;
		}
		String s = ((minutes==0)?":":minutes+":") + ((seconds<=9)?"0"+seconds:seconds);
		Devices.getInstance().getIndicator().set(s);
	}
	
	@Override
	public void indicateNumber(int num) {
		Devices.getInstance().getIndicator().set(Integer.toString(num));
	}
	
	@Override
	public void indicateString(String s) {
		Devices.getInstance().getIndicator().set(s);
	}
	
	@Override
	public void indicateMode(String name, int val) {
		Devices.getInstance().getIndicator().set(name + " - " + val);
	}
	
	@Override
	public void reset() {
		Devices.getInstance().getIndicator().set("<time>");
	}
	
}
