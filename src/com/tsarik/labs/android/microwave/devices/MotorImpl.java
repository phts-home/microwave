package com.tsarik.labs.android.microwave.devices;

import com.tsarik.labs.android.microwave.layout.Layout;
import com.tsarik.labs.android.microwave.layout.LayoutManager;


/**
 * Simple implementation for the <code>Motor</code> interface.
 * 
 * @author Phil Tsarik
 *
 */
class MotorImpl implements Motor {
	
	private boolean switchedOn;
	private double speed;
	
	
	public MotorImpl() {
		switchOff();
	}
	

	@Override
	public double getSpeed() {
		return speed;
	}

	@Override
	public boolean isSwitchedOn() {
		return switchedOn;
	}

	@Override
	public void setSpeed(double speed) {
		this.speed = speed;
		if (switchedOn) {
			LayoutManager.getLayout().print(Layout.TYPE_MOTOR, "On, Speed="+speed, "#FFFF33");
		}
	}

	@Override
	public void switchOff() {
		switchedOn = false;
		speed = 0;
		LayoutManager.getLayout().print(Layout.TYPE_MOTOR, "Off", "#777766");
	}

	@Override
	public void switchOn() {
		if (speed == 0) {
			switchOff();
		}
		switchedOn = true;
		LayoutManager.getLayout().print(Layout.TYPE_MOTOR, "On, Speed="+speed, "#FFFF33");
	}
	
}
