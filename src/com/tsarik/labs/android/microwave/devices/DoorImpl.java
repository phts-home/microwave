package com.tsarik.labs.android.microwave.devices;

import com.tsarik.labs.android.microwave.layout.Layout;
import com.tsarik.labs.android.microwave.layout.LayoutManager;


/**
 * Simple implementation for the <code>Door</code> interface.
 * 
 * @author Phil Tsarik
 *
 */
class DoorImpl implements Door {
	
	private boolean opened;
	
	
	
	public DoorImpl() {
		close();
	}
	
	@Override
	public boolean isOpen() {
		return opened;
	}

	@Override
	public void close() {
		opened = false;
		LayoutManager.getLayout().print(Layout.TYPE_DOOR, "Closed");
	}
	
	@Override
	public void open() {
		opened = true;
		LayoutManager.getLayout().print(Layout.TYPE_DOOR, "Opened");
	}

}
