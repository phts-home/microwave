package com.tsarik.labs.android.microwave.devices;

import com.tsarik.labs.android.microwave.layout.Layout;
import com.tsarik.labs.android.microwave.layout.LayoutManager;


/**
 * Simple implementation for the <code>Magnetron</code> interface.
 * 
 * @author Phil Tsarik
 *
 */
class MagnetronImpl implements Magnetron {
	
	private boolean switchedOn;
	private double power;
	
	
	public MagnetronImpl() {
		switchOff();
	}
	
	
	@Override
	public double getPower() {
		return power;
	}

	@Override
	public boolean isSwitchedOn() {
		return switchedOn;
	}

	@Override
	public void setPower(double power) {
		this.power = power;
		if (switchedOn) {
			LayoutManager.getLayout().print(Layout.TYPE_MAGNETRON, "On, Power="+power, "#FFFF33");
		}
	}

	@Override
	public void switchOff() {
		switchedOn = false;
		power = 0;
		LayoutManager.getLayout().print(Layout.TYPE_MAGNETRON, "Off", "#777766");
	}

	@Override
	public void switchOn() {
		if (power == 0) {
			switchOff();
		}
		switchedOn = true;
		LayoutManager.getLayout().print(Layout.TYPE_MAGNETRON, "On, Power="+power, "#FFFF33");
	}
	
}
