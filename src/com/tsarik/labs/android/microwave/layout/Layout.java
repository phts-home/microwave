package com.tsarik.labs.android.microwave.layout;


/**
 * Interface for the ui layout. 
 * 
 * @author Phil Tsarik
 *
 */
public interface Layout {
	
	public static final String TYPE_KEY = "key";
	public static final String TYPE_INDICATOR = "indicator";
	public static final String TYPE_BUTTON = "button";
	public static final String TYPE_STATE = "state";
	public static final String TYPE_USER = "user";
	public static final String TYPE_DOOR = "door";
	public static final String TYPE_LIGHT = "light";
	public static final String TYPE_MAGNETRON = "magnetron";
	public static final String TYPE_MOTOR = "motor";
	public static final String TYPE_FAN = "fan";
	public static final String TYPE_SPEAKER = "speaker";
	public static final String TYPE_COUNTER = "counter";
	
	
	/**
	 * Sends the specified message message onto the layout to print or something else
	 * 
	 * @param type Message type
	 * @param message Message object
	 */
	public void print(String type, Object message);
	
	/**
	 * Sends the specified message onto the layout with the specified color
	 * 
	 * @param type Message type
	 * @param message Message object
	 * @param colorString Color string
	 */
	public void print(String type, Object message, String colorString);
	
	/**
	 * Sets the touch screen handler to handle clicking on the layout's <code>View</code>s
	 * 
	 * @param handler TouchScreenHandler implementation to set
	 */
	public void setTouchScreenHandler(TouchScreenHandler handler);
	
}
