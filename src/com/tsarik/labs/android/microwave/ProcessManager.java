package com.tsarik.labs.android.microwave;

import com.tsarik.labs.android.microwave.devices.Devices;
import com.tsarik.labs.android.microwave.layout.Layout;
import com.tsarik.labs.android.microwave.layout.LayoutManager;


/**
 * Contains main microwave's logic. 
 * It has access to system devices through the linked <code>Devices</code> object.
 * 
 * @author Phil Tsarik
 *
 */
public class ProcessManager {
	
	private final int[] powerValues = {300, 450, 600, 750};
	private final int[] timeValues = {10, 20, 30, 40, 50, 60, 90, 120, 180, 240, 300, 360, 420, 480, 540, 600, 900, 1200};
	private final int[][] modesValues = { {1, 2, 3}, {10, 20}, {100, 200, 300} };
	private final String[] modeNames = {"Reheat", "Cook", "Defrost"};
	
	public static final int MODE_1 = 0;
	public static final int MODE_2 = 1;
	public static final int MODE_3 = 2;
	
	private final int STATE_WAITING = 0;
	private final int STATE_WORKING = 1;
	private final int STATE_SETUP = 2;
	private final int STATE_PAUSED = 3;
	private final int STATE_MODE1 = 4;
	private final int STATE_MODE2 = 5;
	private final int STATE_MODE3 = 6;
	
	private final String[] stateNames = {"Waiting", "Working", "Setup", "Paused", "Mode#1", "Mode#2" , "Mode#3"};
	
	private ProcessingThread thread;
	
	private int state;
	private int powerValueIndex = 0;
	private int timeValueIndex = 0;
	private int[] modeValueIndexes = {0, 0, 0};
	private int curPower = 0;
	private int curTime = 0;
	
	
	
	public ProcessManager() {
		reset();
	}
	
	
	public void start() {
		if (Devices.getInstance().getDoor().isOpen()) {
			Devices.getInstance().getSpeakerAdapter().playButtonSound();
			return;
		}
		switch (state) {
		case STATE_WAITING:
			Devices.getInstance().getSpeakerAdapter().playButtonSound();
			start(750, 30);
			break;
		case STATE_WORKING:
			Devices.getInstance().getSpeakerAdapter().playButtonSound();
			addTime(30);
			break;
		case STATE_SETUP:
			Devices.getInstance().getSpeakerAdapter().playButtonSound();
			start(powerValues[powerValueIndex], timeValues[timeValueIndex]);
			break;
		case STATE_PAUSED:
			Devices.getInstance().getSpeakerAdapter().playButtonSound();
			resume();
			break;
		case STATE_MODE1:
			startMode(MODE_1);
			break;
		case STATE_MODE2:
			startMode(MODE_2);
			break;
		case STATE_MODE3:
			startMode(MODE_3);
			break;
		}
		
	}
	
	public void stop() {
		switch (state) {
		case STATE_WAITING:
			Devices.getInstance().getSpeakerAdapter().playButtonSound();
			break;
		case STATE_WORKING:
			Devices.getInstance().getSpeakerAdapter().playButtonSound();
			pause();
			break;
		case STATE_SETUP:
		case STATE_PAUSED:
		case STATE_MODE1:
		case STATE_MODE2:
		case STATE_MODE3:
			Devices.getInstance().getSpeakerAdapter().playButtonSound();
			reset();
			break;
		}
		
	}
	
	public void mode(int mode) {
		if (state != STATE_WAITING) {
			return;
		}
		
		switch (mode) {
		case MODE_1:
			setState(STATE_MODE1);
			setModeValueIndex(MODE_1, 0);
			Devices.getInstance().getIndicatorAdater().indicateMode(modeNames[mode], modesValues[mode][modeValueIndexes[mode]]);
			break;
		case MODE_2:
			setState(STATE_MODE2);
			setModeValueIndex(MODE_2, 0);
			Devices.getInstance().getIndicatorAdater().indicateMode(modeNames[mode], modesValues[mode][modeValueIndexes[mode]]);
			break;
		case MODE_3:
			setState(STATE_MODE3);
			setModeValueIndex(MODE_3, 0);
			Devices.getInstance().getIndicatorAdater().indicateMode(modeNames[mode], modesValues[mode][modeValueIndexes[mode]]);
			break;
		}
	}
	
	public void openDoor() {
		if (state == STATE_WORKING) {
			pause();
			while (thread != null) {
				// wait until thread ends its work
			}
		}
		Devices.getInstance().getDoor().open();
		Devices.getInstance().getLight().switchOn();
		
	}
	
	public void closeDoor() {
		if (Devices.getInstance().getDoor().isOpen()) {
			Devices.getInstance().getDoor().close();
			Devices.getInstance().getLight().switchOff();
		}
	}
	
	public void incPower() {
		switch (state) {
		case STATE_WAITING:
			setState(STATE_SETUP);
			Devices.getInstance().getIndicatorAdater().indicateNumber(powerValues[powerValueIndex]);
			break;
		case STATE_SETUP:
			Devices.getInstance().getSpeakerAdapter().playButtonSound();
			setPowerValueIndex(powerValueIndex + 1);
			Devices.getInstance().getIndicatorAdater().indicateNumber(powerValues[powerValueIndex]);
			break;
		case STATE_MODE1:
			Devices.getInstance().getSpeakerAdapter().playButtonSound();
			setModeValueIndex(MODE_1, modeValueIndexes[MODE_1]+1);
			Devices.getInstance().getIndicatorAdater().indicateMode(modeNames[MODE_1], modesValues[MODE_1][modeValueIndexes[MODE_1]]);
			break;
		case STATE_MODE2:
			Devices.getInstance().getSpeakerAdapter().playButtonSound();
			setModeValueIndex(MODE_2, modeValueIndexes[MODE_2]+1);
			Devices.getInstance().getIndicatorAdater().indicateMode(modeNames[MODE_2], modesValues[MODE_2][modeValueIndexes[MODE_2]]);
			break;
		case STATE_MODE3:
			Devices.getInstance().getSpeakerAdapter().playButtonSound();
			setModeValueIndex(MODE_3, modeValueIndexes[MODE_3]+1);
			Devices.getInstance().getIndicatorAdater().indicateMode(modeNames[MODE_3], modesValues[MODE_3][modeValueIndexes[MODE_3]]);
			break;
		}
	}
	
	public void decPower() {
		switch (state) {
		case STATE_WAITING:
			setState(STATE_SETUP);
			Devices.getInstance().getIndicatorAdater().indicateNumber(powerValues[powerValueIndex]);
			break;
		case STATE_SETUP:
			Devices.getInstance().getSpeakerAdapter().playButtonSound();
			setPowerValueIndex(powerValueIndex - 1);
			Devices.getInstance().getIndicatorAdater().indicateNumber(powerValues[powerValueIndex]);
			break;
		case STATE_MODE1:
			Devices.getInstance().getSpeakerAdapter().playButtonSound();
			setModeValueIndex(MODE_1, modeValueIndexes[MODE_1]-1);
			Devices.getInstance().getIndicatorAdater().indicateMode(modeNames[MODE_1], modesValues[MODE_1][modeValueIndexes[MODE_1]]);
			break;
		case STATE_MODE2:
			Devices.getInstance().getSpeakerAdapter().playButtonSound();
			setModeValueIndex(MODE_2, modeValueIndexes[MODE_2]-1);
			Devices.getInstance().getIndicatorAdater().indicateMode(modeNames[MODE_2], modesValues[MODE_2][modeValueIndexes[MODE_2]]);
			break;
		case STATE_MODE3:
			Devices.getInstance().getSpeakerAdapter().playButtonSound();
			setModeValueIndex(MODE_3, modeValueIndexes[MODE_3]-1);
			Devices.getInstance().getIndicatorAdater().indicateMode(modeNames[MODE_3], modesValues[MODE_3][modeValueIndexes[MODE_3]]);
			break;
		}
	}
	
	public void incTime() {
		switch (state) {
		case STATE_WAITING:
			setState(STATE_SETUP);
			Devices.getInstance().getIndicatorAdater().indicateTime(timeValues[timeValueIndex]);
			break;
		case STATE_SETUP:
			Devices.getInstance().getSpeakerAdapter().playButtonSound();
			setTimeValueIndex(timeValueIndex + 1);
			Devices.getInstance().getIndicatorAdater().indicateTime(timeValues[timeValueIndex]);
			break;
		}
	}
	
	public void decTime() {
		switch (state) {
		case STATE_WAITING:
			setState(STATE_SETUP);
			Devices.getInstance().getIndicatorAdater().indicateTime(timeValues[timeValueIndex]);
		case STATE_SETUP:
			Devices.getInstance().getSpeakerAdapter().playButtonSound();
			setTimeValueIndex(timeValueIndex - 1);
			Devices.getInstance().getIndicatorAdater().indicateTime(timeValues[timeValueIndex]);
			break;
		}
	}
	

	
	
	private void reset() {
		setPowerValueIndex(0);
		setTimeValueIndex(0);
		curTime = 0;
		curPower = 0;
		setState(STATE_WAITING);
		Devices.getInstance().getIndicatorAdater().reset();
	}
	
	private void start(int power, int sec) {
		thread = new ProcessingThread();
		thread.setTimeinSec(sec);
		thread.setPower(power);
		thread.start();
		setState(STATE_WORKING);
	}
	
	private void addTime(int sec) {
		if (thread != null) {
			thread.addTime(sec);
		}
	}
	
	private void resume() {
		thread = new ProcessingThread();
		thread.setTimeinSec(curTime);
		thread.setPower(curPower);
		thread.start();
		setState(STATE_WORKING);
	}
	
	private void pause() {
		if (thread != null) {
			curPower = thread.getPower();
			curTime = thread.getTimeInSec();
			thread.stopProcessing();
		}
		setState(STATE_PAUSED);
	}
	
	private void startMode(int mode) {
		switch (mode) {
		case MODE_1: // Reheat
			switch (modeValueIndexes[0]) {
			case 0:
				start(450, 200);
				break;
			case 1:
				start(500, 250);
				break;
			case 2:
				start(550, 300);
				break;
			}
			break;
		case MODE_2: // Cook
			switch (modeValueIndexes[1]) {
			case 0:
				start(600, 1025);
				break;
			case 1:
				start(700, 600);
				break;
			}
			break;
		case MODE_3: // Defrost
			switch (modeValueIndexes[2]) {
			case 0:
				start(150, 700);
				break;
			case 1:
				start(200, 800);
				break;
			case 2:
				start(250, 900);
				break;
			}
			break;
		}
		
	}
	
	
	
	private void setState(int state) {
		this.state = state;
		LayoutManager.getLayout().print(Layout.TYPE_STATE, stateNames[state]);
		
		if (state == STATE_WAITING) {
			LayoutManager.getLayout().print(Layout.TYPE_USER, null);
		}
	}
	
	private void setPowerValueIndex(int newPowerValueIndex) {
		if (newPowerValueIndex > powerValues.length-1 || newPowerValueIndex < 0) {
			return;
		}
		this.powerValueIndex = newPowerValueIndex;
		LayoutManager.getLayout().print(Layout.TYPE_USER, "Power=" + powerValues[newPowerValueIndex] + ", Time=" + timeValues[timeValueIndex]);
	}
	
	private void setTimeValueIndex(int newTimeValueIndex) {
		if (newTimeValueIndex > timeValues.length-1 || newTimeValueIndex < 0) {
			return;
		}
		this.timeValueIndex = newTimeValueIndex;
		LayoutManager.getLayout().print(Layout.TYPE_USER, "Power=" + powerValues[powerValueIndex] + ", Time=" + timeValues[newTimeValueIndex]);
	}
	
	private void setModeValueIndex(int mode, int newModeValueIndex) {
		if (newModeValueIndex > modesValues[mode].length-1 || newModeValueIndex < 0) {
			return;
		}
		this.modeValueIndexes[mode] = newModeValueIndex;
	}
	
	
	
	
	
	
	/**
	 * Class for processing thread.
	 * 
	 * @author Phil Tsarik
	 *
	 */
	private class ProcessingThread extends Thread {
		
		private int time = 0;
		private int power = 0;
		private boolean stopped = false;
		
		
		public void setTimeinSec(int sec) {
			setTime(sec*10);
		}
		
		public int getTimeInSec() {
			return Double.valueOf(Math.ceil(time/10.0)).intValue();
		}
		
		public void setPower(int power) {
			this.power = power;
		}
		
		public int getPower() {
			return power;
		}
		
		public void addTime(int sec) {
			setTime(this.time + sec*10);
		}
		
		public void stopProcessing() {
			stopped = true;
		}
		
		
		
		private void beginProcessing() {
			Devices.getInstance().getLight().switchOn();
			Devices.getInstance().getMotor().setSpeed(66.6);
			Devices.getInstance().getMotor().switchOn();
			Devices.getInstance().getFan().setSpeed(111.1);
			Devices.getInstance().getFan().switchOn();
			Devices.getInstance().getMagnetron().setPower(power);
			Devices.getInstance().getMagnetron().switchOn();
		}
		
		private void finishProcessing() {
			Devices.getInstance().getMagnetron().switchOff();
			Devices.getInstance().getFan().switchOff();
			Devices.getInstance().getMotor().switchOff();
			Devices.getInstance().getLight().switchOff();
			
			Devices.getInstance().getSpeakerAdapter().playFinishProcessingSound();
			
			reset();
			
			LayoutManager.getLayout().print(Layout.TYPE_COUNTER, null);
		}
		
		private void cancelProcessing() {
			Devices.getInstance().getMagnetron().switchOff();
			Devices.getInstance().getFan().switchOff();
			Devices.getInstance().getMotor().switchOff();
			Devices.getInstance().getLight().switchOff();
			
			LayoutManager.getLayout().print(Layout.TYPE_COUNTER, null);
		}
		
		private void setTime(int time) {
			this.time = time;
			Devices.getInstance().getIndicatorAdater().indicateTime(getTimeInSec());
			LayoutManager.getLayout().print(Layout.TYPE_COUNTER, time);
		}
		
		
		
		@Override
		public void run() {
			if (time <= 0) {
				reset();
				return;
			}
			beginProcessing();
			while (time > 0) {
				LayoutManager.getLayout().print(Layout.TYPE_COUNTER, time);
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// nothing
				}
				if (stopped) {
					break;
				}
				setTime(time - 1);
			}
			if (stopped) {
				cancelProcessing();
			} else {
				finishProcessing();
			}
			
			thread = null;
		}
		
		
		
	}
	
	
	
}
