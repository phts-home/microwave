package com.tsarik.labs.android.microwave.devices;


/**
 * Interface for the Speaker.
 * 
 * @author Phil Tsarik
 *
 */
public interface Speaker {
	
	/**
	 * Returns <code>true</code> if the speaker is switched on.
	 * 
	 * @return <code>true</code> if the speaker is switched on or <code>false</code> otherwise
	 */
	public boolean isSwitchedOn();
	
	/**
	 * Gets current frequency of the speaker.
	 * 
	 * @return Current frequency of the speaker or 0 if the speaker is switched off
	 */
	public int getFreq();
	
	/**
	 * Switches on the speaker with the specified frequency.
	 * 
	 * @param freq The frequency to set
	 */
	public void switchOn(int freq);
	
	/**
	 * Switches off the speaker.
	 */
	public void switchOff();
	
}
