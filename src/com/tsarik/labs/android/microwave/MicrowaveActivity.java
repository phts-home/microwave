package com.tsarik.labs.android.microwave;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;

import com.tsarik.labs.android.microwave.devices.Devices;
import com.tsarik.labs.android.microwave.layout.LayoutManager;


/**
 * Main application's activity.
 * 
 * @author Phil Tsarik
 *
 */
public class MicrowaveActivity extends Activity {
	
	private UserInputHandler userInputHandler;
	private ProcessManager processManager;
	
	
	/**
	 * Called when the activity is first created.
	 * 
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		LayoutManager.setLayout(this, "front");
		
		Devices.createInstance();
		
		processManager = new ProcessManager();
		userInputHandler = new UserInputHandler(this, processManager);
		
		LayoutManager.getLayout().setTouchScreenHandler(userInputHandler);
		
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return userInputHandler.handleKey(keyCode, event);
	}
	
}