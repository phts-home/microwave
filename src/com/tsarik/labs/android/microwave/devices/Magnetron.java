package com.tsarik.labs.android.microwave.devices;


/**
 * Interface for the Magnetron.
 * 
 * @author Phil Tsarik
 *
 */
public interface Magnetron {
	
	/**
	 * Switches on the magnetron.
	 */
	public void switchOn();
	
	/**
	 * Switches off the magnetron.
	 */
	public void switchOff();
	
	/**
	 * Returns <code>true</code> if the magnetron is switched on.
	 * 
	 * @return <code>true</code> if the magnetron is switched on or <code>false</code> otherwise
	 */
	public boolean isSwitchedOn();
	
	/**
	 * Sets the specified power for the magnetron.
	 * 
	 * @param speed Power value to set or 0 to switch off
	 */
	public void setPower(double power);
	
	/**
	 * Gets current power of the magnetron.
	 * 
	 * @return Current speed of the fan or 0 if the magnetron is switched off
	 */
	public double getPower();
	
}
