package com.tsarik.labs.android.microwave;

import android.app.Activity;
import android.view.KeyEvent;

import com.tsarik.labs.android.microwave.R;
import com.tsarik.labs.android.microwave.layout.Layout;
import com.tsarik.labs.android.microwave.layout.LayoutManager;
import com.tsarik.labs.android.microwave.layout.TouchScreenHandler;

/**
 * Handles pressed buttons and sends commands to the linked <code>ProcessManager</code> object.
 * 
 * @author Phil Tsarik
 * 
 */
public class UserInputHandler implements TouchScreenHandler {

	private ProcessManager processManager;
	private Activity activity;

	public UserInputHandler(Activity activity, ProcessManager processManager) {
		this.processManager = processManager;
		this.activity = activity;
	}

	/**
	 * @return <code>true</code> if this key was handled, <code>false</code> otherwise
	 */
	public boolean handleKey(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case 16: // 9 - front layout
			LayoutManager.setLayout(activity, "front");
			LayoutManager.getLayout().setTouchScreenHandler(this);
			return true;
		case 7: // 0 - debug layout
			LayoutManager.setLayout(activity, "debug");
			LayoutManager.getLayout().setTouchScreenHandler(this);
			return true;
		}
		
		LayoutManager.getLayout().print(Layout.TYPE_KEY, keyCode);
		switch (keyCode) {
		case 8: // 1 - mode1
			LayoutManager.getLayout().print(Layout.TYPE_BUTTON, "Mode 1");
			processManager.mode(ProcessManager.MODE_1);
			return true;
		case 9: // 2 - mode2
			LayoutManager.getLayout().print(Layout.TYPE_BUTTON, "Mode 2");
			processManager.mode(ProcessManager.MODE_2);
			return true;
		case 10: // 3 - mode3
			LayoutManager.getLayout().print(Layout.TYPE_BUTTON, "Mode 3");
			processManager.mode(ProcessManager.MODE_3);
			return true;
		case 62: // space - open door
			LayoutManager.getLayout().print(Layout.TYPE_BUTTON, "Open door");
			processManager.openDoor();
			return true;
		case 76: // / - close door
			LayoutManager.getLayout().print(Layout.TYPE_BUTTON, "Close door");
			processManager.closeDoor();
			return true;
		case 45: // Q - Power-
			LayoutManager.getLayout().print(Layout.TYPE_BUTTON, "Power -");
			processManager.decPower();
			return true;
		case 51: // W - Power+
			LayoutManager.getLayout().print(Layout.TYPE_BUTTON, "Power +");
			processManager.incPower();
			return true;
		case 29: // A - Time-
			LayoutManager.getLayout().print(Layout.TYPE_BUTTON, "Time -");
			processManager.decTime();
			return true;
		case 47: // S - Time+
			LayoutManager.getLayout().print(Layout.TYPE_BUTTON, "Time +");
			processManager.incTime();
			return true;
		case 66: // Enter - Start
			LayoutManager.getLayout().print(Layout.TYPE_BUTTON, "Start");
			processManager.start();
			return true;
		case 67: // Backspace - Stop
			LayoutManager.getLayout().print(Layout.TYPE_BUTTON, "Stop");
			processManager.stop();
			return true;
		}
		LayoutManager.getLayout().print(Layout.TYPE_BUTTON, "");
		return false;
	}
	
	@Override
	public boolean handleViewClick(int id) {
		switch (id) {
		case R.id.front_button_start:
			processManager.start();
			return true;
		case R.id.front_button_stop:
			processManager.stop();
			return true;
		case R.id.front_button_open:
			processManager.openDoor();
			return true;
		case R.id.front_button_close:
			processManager.closeDoor();
			return true;
		case R.id.front_button_incpower:
			processManager.incPower();
			return true;
		case R.id.front_button_decpower:
			processManager.decPower();
			return true;
		case R.id.front_button_inctime:
			processManager.incTime();
			return true;
		case R.id.front_button_dectime:
			processManager.decTime();
			return true;
		case R.id.front_button_mode1:
			processManager.mode(ProcessManager.MODE_1);
			return true;
		case R.id.front_button_mode2:
			processManager.mode(ProcessManager.MODE_2);
			return true;
		case R.id.front_button_mode3:
			processManager.mode(ProcessManager.MODE_3);
			return true;
		}
		return false;
	}
	
}
