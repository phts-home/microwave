package com.tsarik.labs.android.microwave.devices;


/**
 * Interface for the Indicator.
 * 
 * @author Phil Tsarik
 *
 */
public interface Indicator {
	
	/**
	 * Sets some text to indicate.
	 *  
	 * @param value Text to indicate
	 */
	public void set(String value);
	
}
