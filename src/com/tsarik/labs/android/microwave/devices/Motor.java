package com.tsarik.labs.android.microwave.devices;


/**
 * Interface for the Motor.
 * 
 * @author Phil Tsarik
 *
 */
public interface Motor {
	
	/**
	 * Switches on the motor.
	 */
	public void switchOn();
	
	/**
	 * Switches off the motor.
	 */
	public void switchOff();
	
	/**
	 * Returns <code>true</code> if the motor is switched on.
	 * 
	 * @return <code>true</code> if the motor is switched on or <code>false</code> otherwise
	 */
	public boolean isSwitchedOn();
	
	/**
	 * Sets the specified speed for the motor.
	 * 
	 * @param speed Speed value to set or 0 to switch off
	 */
	public void setSpeed(double speed);
	
	/**
	 * Gets current speed of the motor.
	 * 
	 * @return Current speed of the motor or 0 if the motor is switched off
	 */
	public double getSpeed();
	
}
