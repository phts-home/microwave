package com.tsarik.labs.android.microwave.devices;

import com.tsarik.labs.android.microwave.layout.Layout;
import com.tsarik.labs.android.microwave.layout.LayoutManager;


/**
 * Simple implementation for the <code>Indicator</code> interface.
 * 
 * @author Phil Tsarik
 *
 */
class IndicatorImpl implements Indicator {
	
	@Override
	public void set(String value) {
		LayoutManager.getLayout().print(Layout.TYPE_INDICATOR, value);
	}
	
}
