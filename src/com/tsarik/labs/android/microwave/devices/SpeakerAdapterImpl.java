package com.tsarik.labs.android.microwave.devices;


/**
 * Simple implementation for the <code>SpeakerAdapter</code> interface.
 * 
 * @author Phil Tsarik
 *
 */
class SpeakerAdapterImpl implements SpeakerAdapter {
	
	private ButtonSoundPlayingThread buttonSoundPlayingThread;
	private FinishProcessingSoundPlayingThread finishProcessingSoundPlayingThread;
	
	@Override
	public void playButtonSound() {
		// TODO: stop prev thread
		buttonSoundPlayingThread = new ButtonSoundPlayingThread();
		buttonSoundPlayingThread.start();
	}

	@Override
	public void playFinishProcessingSound() {
		// TODO: stop prev thread
		finishProcessingSoundPlayingThread = new FinishProcessingSoundPlayingThread();
		finishProcessingSoundPlayingThread.start();
	}
	
	
	
	
	
	private class ButtonSoundPlayingThread extends Thread {
		
		@Override
		public void run() {
			Devices.getInstance().getSpeaker().switchOn(10000);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			Devices.getInstance().getSpeaker().switchOff();
		}
		
	}
	
	private class FinishProcessingSoundPlayingThread extends Thread {
		
		@Override
		public void run() {
			for (int i = 0; i < 3; i++) {
				Devices.getInstance().getSpeaker().switchOn(10000);
				Devices.getInstance().getIndicatorAdater().indicateTime(0);
				try {
					Thread.sleep(400);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				Devices.getInstance().getSpeaker().switchOff();
				Devices.getInstance().getIndicatorAdater().clear();
				try {
					Thread.sleep(400);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

}
