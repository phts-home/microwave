package com.tsarik.labs.android.microwave.layout;


/**
 * Interface for the touch screen handler. Provides 
 * handling <code>view</code>'s clicks and click with coordinates.
 * 
 * @author Phil Tsarik
 *
 */
public interface TouchScreenHandler {
	
	/**
	 * Handles click on the <code>View</code>s
	 * 
	 * @param id <code>View</code>'s id
	 * @return <code>true</code> if this click was handled or <code>false</code> otherwise
	 */
	public boolean handleViewClick(int id);
	
}
