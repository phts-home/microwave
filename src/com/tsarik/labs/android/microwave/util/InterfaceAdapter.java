package com.tsarik.labs.android.microwave.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;


/**
 * Provides linking to the real devices through the file interface.
 * 
 * @author Phil Tsarik
 *
 */
public class InterfaceAdapter {
	
	public static final String INTERFACE_ROOT = "/sys/class/microwave/";
	
	public static void writeString(String dev, String param, String value) {
		try {
			(new File(INTERFACE_ROOT + dev + "/")).mkdirs();
			FileWriter fw = new FileWriter(INTERFACE_ROOT + dev + "/" + param);
			fw.write(value);
			fw.close();
		} catch (Exception e) {
			// cannot write
			e.printStackTrace();
		}
	}
	
	public static void writeInteger(String dev, String param, Integer value) {
		writeString(dev, param, Integer.toString(value));
	}
	
	public static String readString(String dev, String param) {
		String s = null;
		try {
			BufferedReader bf = new BufferedReader(new FileReader(INTERFACE_ROOT + dev + "/" + param));
			s = bf.readLine();
		} catch (Exception e) {
			// cannot read
		}
		return s;
	}
	
	public static Integer readInteger(String dev, String param) {
		String s = readString(dev, param);
		int r = 0;
		if (s == null) {
			return r;
		}
		try {
			r = Integer.parseInt(s);
		} catch (Exception e) {
			// cannot parse
		}
		return r;
	}
	
}
