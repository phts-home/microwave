package com.tsarik.labs.android.microwave.layout;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.tsarik.labs.android.microwave.R;


/**
 * Describes pretty UI with images and buttons. 
 * 
 * @author Phil Tsarik
 *
 */
class FrontLayout implements Layout {
	
	private Activity activity;
	
	public FrontLayout(Activity activity) {
		this.activity = activity;
		
		activity.setContentView(R.layout.front);
		
		activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}
	
	
	@Override
	public void setTouchScreenHandler(final TouchScreenHandler handler) {
		Button button = (Button) activity.findViewById(R.id.front_button_start);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				handler.handleViewClick(R.id.front_button_start);
			}
		});
		
		button = (Button) activity.findViewById(R.id.front_button_stop);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				handler.handleViewClick(R.id.front_button_stop);
			}
		});
		
		button = (Button) activity.findViewById(R.id.front_button_open);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				handler.handleViewClick(R.id.front_button_open);
			}
		});
		
		button = (Button) activity.findViewById(R.id.front_button_close);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				handler.handleViewClick(R.id.front_button_close);
			}
		});
		
		button = (Button) activity.findViewById(R.id.front_button_incpower);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				handler.handleViewClick(R.id.front_button_incpower);
			}
		});
		
		button = (Button) activity.findViewById(R.id.front_button_decpower);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				handler.handleViewClick(R.id.front_button_decpower);
			}
		});
		
		button = (Button) activity.findViewById(R.id.front_button_inctime);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				handler.handleViewClick(R.id.front_button_inctime);
			}
		});
		
		button = (Button) activity.findViewById(R.id.front_button_dectime);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				handler.handleViewClick(R.id.front_button_dectime);
			}
		});
		
		button = (Button) activity.findViewById(R.id.front_button_mode1);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				handler.handleViewClick(R.id.front_button_mode1);
			}
		});
		
		button = (Button) activity.findViewById(R.id.front_button_mode2);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				handler.handleViewClick(R.id.front_button_mode2);
			}
		});
		
		button = (Button) activity.findViewById(R.id.front_button_mode3);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				handler.handleViewClick(R.id.front_button_mode3);
			}
		});
	}
	
	
	// use Handler because we cannot update UI objects while in a separate thread
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Bundle data = msg.getData();
			String type = data.getString("type");
			String message = data.getString("message");
			
			if (type.equalsIgnoreCase(TYPE_INDICATOR)) {
				((EditText)activity.findViewById(R.id.front_text_indicator)).setText(message);
			} else if (type.equalsIgnoreCase(TYPE_LIGHT)) {
				if (message.equalsIgnoreCase("on")) {
					activity.findViewById(R.id.front_layout_light).setBackgroundResource(R.drawable.light_on);
				} else {
					activity.findViewById(R.id.front_layout_light).setBackgroundResource(R.drawable.light_off);
				}
			} else if (type.equalsIgnoreCase(TYPE_DOOR)) {
				if (message.equalsIgnoreCase("closed")) {
					activity.findViewById(R.id.front_image_door).setBackgroundResource(R.drawable.door_closed);
				} else {
					activity.findViewById(R.id.front_image_door).setBackgroundResource(0);
				}
			} else {
				return;
			}
		}
	};
	
	
	@Override
	public void print(String type, Object message, String colorString) {
		// build message and send it
		Bundle data = new Bundle();
		data.putString("type", type);
		if (message != null) {
			data.putString("message", message.toString());
		}
		if (colorString != null) {
			data.putString("colorString", colorString);
		}
		
		Message msg = Message.obtain();
		msg.setData(data);
		
		handler.sendMessage(msg);
	}
	
	@Override
	public void print(String type, Object message) {
		print(type, message, null);
	}
	
}
