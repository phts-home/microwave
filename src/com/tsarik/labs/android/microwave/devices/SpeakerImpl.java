package com.tsarik.labs.android.microwave.devices;

import com.tsarik.labs.android.microwave.layout.Layout;
import com.tsarik.labs.android.microwave.layout.LayoutManager;

class SpeakerImpl implements Speaker {
	
	private boolean switchedOn;
	private int freq;
	
	
	public SpeakerImpl() {
		switchOff();
	}
	

	@Override
	public int getFreq() {
		return freq;
	}

	@Override
	public boolean isSwitchedOn() {
		return switchedOn;
	}

	@Override
	public void switchOff() {
		switchedOn = false;
		freq = 0;
		LayoutManager.getLayout().print(Layout.TYPE_SPEAKER, "Off", "#777766");
	}

	@Override
	public void switchOn(int freq) {
		if (freq == 0) {
			switchOff();
		}
		this.freq = freq;
		switchedOn = true;
		LayoutManager.getLayout().print(Layout.TYPE_SPEAKER, "On, Freq="+freq, "#FFFF33");
	}

}
