package com.tsarik.labs.android.microwave.devices;


/**
 * Interface for the Fan.
 * 
 * @author Phil Tsarik
 *
 */
public interface Fan {
	
	/**
	 * Switches on the fan.
	 */
	public void switchOn();
	
	/**
	 * Switches off the fan.
	 */
	public void switchOff();
	
	/**
	 * Returns <code>true</code> if the fan is switched on.
	 * 
	 * @return <code>true</code> if the fan is switched on or <code>false</code> otherwise
	 */
	public boolean isSwitchedOn();
	
	/**
	 * Sets the specified speed for the fan.
	 * 
	 * @param speed Speed value to set or 0 to switch off
	 */
	public void setSpeed(double speed);
	
	/**
	 * Gets current speed of the fan.
	 * 
	 * @return Current speed of the fan or 0 if the fan is switched off
	 */
	public double getSpeed();
	
}
