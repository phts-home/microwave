package com.tsarik.labs.android.microwave.devices;

/**
 * Interface for the Door.
 * 
 * @author Phil Tsarik
 *
 */
public interface Door {
	
	/**
	 * Returns <code>true</code> if the door is open.
	 * 
	 * @return <code>true</code> if the door is open or <code>false</code> otherwise
	 */
	public boolean isOpen();
	
	/**
	 * Closes the door.
	 */
	public void close();
	
	/**
	 * Opens the door.
	 */
	public void open();
	
}
