package com.tsarik.labs.android.microwave.devices;


/**
 * Creates and contains all the devices of the microwave.
 * 
 * @author Phil Tsarik
 *
 */
public class Devices {
	
	private static Devices instance;
	
	public static Devices createInstance() {
		instance = new Devices();
		return instance;
	}
	
	public static Devices getInstance() {
		if (instance == null) {
			return createInstance();
		}
		return instance;
	}
	
	
	
	
	
	private Door door;
	private Fan fan;
	private Indicator indicator;
	private IndicatorAdapter indicatorAdater;
	private Light light;
	private Magnetron magnetron;
	private Motor motor;
	private SpeakerAdapter speakerAdapter;
	private Speaker speaker;
	
	private Devices() {
		this.door = new DoorImpl();
		this.light = new LightImpl();
		this.magnetron = new MagnetronImpl();
		this.motor = new MotorImpl();
		this.fan = new FanImpl();
		this.speakerAdapter = new SpeakerAdapterImpl();
		this.speaker = new SpeakerImpl();
		this.indicator = new IndicatorImpl();
		this.indicatorAdater = new IndicatorAdapterImpl();
	}
	
	
	public Door getDoor() {
		return door;
	}
	
	public Fan getFan() {
		return fan;
	}
	
	public Indicator getIndicator() {
		return indicator;
	}
	
	public IndicatorAdapter getIndicatorAdater() {
		return indicatorAdater;
	}
	
	public Light getLight() {
		return light;
	}
	
	public Magnetron getMagnetron() {
		return magnetron;
	}
	
	public Motor getMotor() {
		return motor;
	}
	
	public SpeakerAdapter getSpeakerAdapter() {
		return speakerAdapter;
	}
	
	public Speaker getSpeaker() {
		return speaker;
	}
	
}
