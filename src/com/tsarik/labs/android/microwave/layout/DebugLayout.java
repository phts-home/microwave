package com.tsarik.labs.android.microwave.layout;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.TextView;

import com.tsarik.labs.android.microwave.R;


/**
 * Describes simple interface containing <code>TextView</code>s. Provides logging and prints logs to the views.
 * 
 * @author Phil Tsarik
 *
 */
class DebugLayout implements Layout {
	
	// use Handler because we cannot update UI objects while in a separate thread
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Bundle data = msg.getData();
			String type = data.getString("type");
			String message = data.getString("message");
			
			int id = 0;
			if (type.equalsIgnoreCase(TYPE_KEY)) {
				id = R.id.text_key;
			} else if (type.equalsIgnoreCase(TYPE_INDICATOR)) {
				id = R.id.text_indicator;
			} else if (type.equalsIgnoreCase(TYPE_BUTTON)) {
				id = R.id.text_button;
			} else if (type.equalsIgnoreCase(TYPE_STATE)) {
				id = R.id.text_state;
			} else if (type.equalsIgnoreCase(TYPE_USER)) {
				id = R.id.text_user;
			} else if (type.equalsIgnoreCase(TYPE_DOOR)) {
				id = R.id.text_door;
			} else if (type.equalsIgnoreCase(TYPE_LIGHT)) {
				id = R.id.text_light;
			} else if (type.equalsIgnoreCase(TYPE_MAGNETRON)) {
				id = R.id.text_magnetron;
			} else if (type.equalsIgnoreCase(TYPE_MOTOR)) {
				id = R.id.text_motor;
			} else if (type.equalsIgnoreCase(TYPE_FAN)) {
				id = R.id.text_fan;
			} else if (type.equalsIgnoreCase(TYPE_SPEAKER)) {
				id = R.id.text_speaker;
			} else if (type.equalsIgnoreCase(TYPE_COUNTER)) {
				id = R.id.text_counter;
			} else {
				id = R.id.text_debug;
			}
			TextView tv = (TextView) activity.findViewById(id);
			if (message == null) {
				tv.setText("");
				return;
			}
			
			tv.setText("["+type+"] " + message);
			
			String colorString = data.getString("colorString");
			if (colorString != null) {
				tv.setTextColor(Color.parseColor(colorString));
			}
		}
	};
	
	private Activity activity;
	
	
	public DebugLayout(Activity activity) {
		this.activity = activity;
		
		activity.setContentView(R.layout.main);
		
		activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}
	
	
	
	
	@Override
	public void print(String type, Object message) {
		print(type, message, null);
	}
	
	@Override
	public void print(String type, Object message, String colorString) {
		// build message and send it
		Bundle data = new Bundle();
		data.putString("type", type);
		if (message != null) {
			data.putString("message", message.toString());
		}
		if (colorString != null) {
			data.putString("colorString", colorString);
		}
		
		Message msg = Message.obtain();
		msg.setData(data);
		
		handler.sendMessage(msg);
	}
	
	@Override
	public void setTouchScreenHandler(final TouchScreenHandler handler) {
		// nothing to handle
	}

	
	
}
