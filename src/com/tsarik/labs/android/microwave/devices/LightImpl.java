package com.tsarik.labs.android.microwave.devices;

import com.tsarik.labs.android.microwave.layout.Layout;
import com.tsarik.labs.android.microwave.layout.LayoutManager;


/**
 * Simple implementation for the <code>Light</code> interface.
 * 
 * @author Phil Tsarik
 *
 */
class LightImpl implements Light {
	
	private boolean switchedOn;
	
	
	public LightImpl() {
		switchOff();
	}
	
	@Override
	public boolean isSwitchedOn() {
		return switchedOn;
	}
	
	@Override
	public void switchOn() {
		switchedOn = true;
		LayoutManager.getLayout().print(Layout.TYPE_LIGHT, "On", "#FFFF33");
	}

	@Override
	public void switchOff() {
		switchedOn = false;
		LayoutManager.getLayout().print(Layout.TYPE_LIGHT, "Off", "#777766");
	}

}
