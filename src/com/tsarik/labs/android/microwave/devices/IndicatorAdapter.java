package com.tsarik.labs.android.microwave.devices;


/**
 * Interface for the Indicator adapter.
 * 
 * @author Phil Tsarik
 *
 */
public interface IndicatorAdapter {
	
	/**
	 * Clears the indicator.
	 */
	public void clear();
	
	/**
	 * Indicates the specified time.
	 * 
	 * @param seconds Time in seconds to indicate
	 */
	public void indicateTime(int seconds);
	
	/**
	 * Indecates the specified number.
	 * 
	 * @param num Number to indicate
	 */
	public void indicateNumber(int num);
	
	/**
	 * Indicates the specified string.
	 * 
	 * @param s String to indicate
	 */
	public void indicateString(String s);
	
	/**
	 * Indicates the specefied mode.
	 * 
	 * @param name Mode name
	 * @param val Mode parameter value
	 */
	public void indicateMode(String name, int val);
	
	/**
	 * Resets the indicator (for example indicates time).
	 */
	public void reset();
	
}
