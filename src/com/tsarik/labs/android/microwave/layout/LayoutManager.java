package com.tsarik.labs.android.microwave.layout;

import android.app.Activity;


/**
 * Manages all the <code>Layout</code>s.
 * 
 * @author Phil Tsarik
 *
 */
public class LayoutManager {
	
	private static Layout layout = null;
	private static boolean creating = false;
	
	/**
	 * Sets the layout for its name.
	 * 
	 * @param activity The activity
	 * @param name The layout's name to set
	 * @return The layout or <code>null</code> if name is unsupported
	 */
	public static Layout setLayout(Activity activity, String name) {
		creating = true;
		layout = null;
		if (name.equalsIgnoreCase("debug")) {
			layout = new DebugLayout(activity);
		} else if (name.equalsIgnoreCase("front")) {
			layout = new FrontLayout(activity);
		}
		creating = false;
		return layout;
	}
	
	/**
	 * Returns current layout.
	 * 
	 * @return The current layout or <code>null</code> if no layout was set
	 */
	public static Layout getLayout() {
		while (creating) {
			// wait while layout is creating in separate thread
		}
		return layout;
	}
	
}
