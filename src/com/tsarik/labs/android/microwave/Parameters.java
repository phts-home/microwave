package com.tsarik.labs.android.microwave;


/**
 * Contains some global software parameters
 * 
 * @author Phil Tsarik
 *
 */
public final class Parameters {
	
	public static final String SOFTWARE_VERSION = "0.8.3";
	
	public static final String COPYRIGHT = "(c) Phil Tsarik, 2010";
	
}
